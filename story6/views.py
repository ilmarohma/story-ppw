from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status
# Create your views here.
def index(request):
	data = Status.objects.all()
	form = StatusForm(request.POST)
	content = {'form' : form, 'data' : data}
	return render(request, 'index.html', content)

def add_status(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			form.save()
	return redirect('/')