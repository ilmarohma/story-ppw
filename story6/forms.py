from django import forms
from .models import Status
from django.forms import ModelForm

class StatusForm(ModelForm):
    class Meta:
        model= Status
        fields = ['status']
        labels = {
            'status':'Status',
        }
        widgets = {
            'status' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '5'}),
        }
