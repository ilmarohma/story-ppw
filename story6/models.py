from django.core.validators import MaxLengthValidator
from django.db import models
import datetime

# Create your models here.


class Status(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=300)


# max_length=300, validators=[MaxLengthValidator(300)]



