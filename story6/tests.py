from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time




class Story6Test(TestCase):
	#general tests
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_url_is_not_exist(self):
		response = Client().get('/invalid')
		self.assertEqual(response.status_code,404)

	def test_using_index_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	#function test
	def test_using_view_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	#model tests
	def test_model_create_new_status(self):
		new_review = Status.objects.create(date=timezone.now(), status='Semangat SIWAK-NYA CUY')
		counting_status_object = Status.objects.all().count()
		self.assertEqual(counting_status_object, 1)

	def test_save_url_is_exist(self):
		response = Client().get('/add_status')
		self.assertEqual(response.status_code,301)

	def test_POST_success_to_save_status(self):
	 	response = Client().post('/add_status/', data={'dates' : '2019-10-10T14:46', 'status' : 'Semangat SIWAK-NYA CUY'})
	 	counting_status_object = Status.objects.all().count()
	 	self.assertEqual(counting_status_object, 1)


	 	self.assertEqual(response.status_code, 302)

	 	new_response = self.client.get('/')
	 	html_response = new_response.content.decode('utf-8')
	 	self.assertIn('Semangat SIWAK-NYA CUY', html_response)



	#forms tests
	def test_forms_validation_for_status_length(self):
		form_data = {'status' : 'Semangat SIWAK-NYA CUY'}
		form = StatusForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_forms_validation_for_status_overlength(self):
		form_data = {'status' : 'Semangat SIWAK-NYA CUY'*500}
		form = StatusForm(data = form_data)
		self.assertFalse(form.is_valid())

	def test_forms_validation_for_status_blank(self):
		form_data = {'status' : ''}
		form = StatusForm(data = form_data)
		self.assertFalse(form.is_valid())


class Story6FuncTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story6FuncTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story6FuncTest, self).tearDown()

	def test_landing_page_element_greet(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(3)

		greet = self.selenium.find_element_by_id('greet')
		self.assertEqual(greet.find_element_by_tag_name('h2').text, 'HALO, APA KABAR?')

	def test_landing_page_status_form(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(3)

		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		message = 'Coba-Coba'
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertIn(message, selenium.page_source)

	def test_landing_page_status_form_more_than_300_char(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(3)

		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		message = 'Coba-Coba'*500
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertNotIn(message, selenium.page_source)

	def test_change_theme(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/story-6/')
		time.sleep(3);
		button = selenium.find_element_by_id('theme')
		button.click()

		body = selenium.find_element_by_tag_name('body')
		self.assertEqual(body.value_of_css_property('background-color'), 'rgb(252, 252, 225)')

		greet = body = selenium.find_element_by_id('greet')
		self.assertEqual(greet.value_of_css_property('background-color'), 'rgb(124, 217, 206)')





